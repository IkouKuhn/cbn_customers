<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $salary
 * @property string $email
 * @property integer $approval
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['approval'], 'integer'],
            [['name', 'city', 'country', 'salary', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'salary' => 'Salary',
            'email' => 'Email',
            'approval' => 'Approval',
        ];
    }
}
