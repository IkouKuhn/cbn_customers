<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'approval'], 'integer'],
            [['name', 'city', 'country', 'salary', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Customer::find();

        $approval = 0;
        if (Yii::$app->user->identity->role == Yii::$app->params['pemeriksaRoleOne']){
            $approval = 1;
        } else if (Yii::$app->user->identity->role == Yii::$app->params['pemeriksaRoleTwo']) {
            $approval = 2;
        } else if (Yii::$app->user->identity->role == Yii::$app->params['managerRole']) {
            $approval = 3;
        }
        $query = Customer::find()->where('approval='.$approval);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'approval' => $this->approval,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'salary', $this->salary])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
