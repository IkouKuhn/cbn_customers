<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */

$approval = 0;
$cancel = 0;
if (Yii::$app->user->identity->role == Yii::$app->params['customerRole']){
    $approval = 1;
} else if (Yii::$app->user->identity->role == Yii::$app->params['pemeriksaRoleOne']){
    $approval = 2;
    $cancel = 0;
} else if (Yii::$app->user->identity->role == Yii::$app->params['pemeriksaRoleTwo']){
    $approval = 3;
    $cancel = 1;
} else if (Yii::$app->user->identity->role == Yii::$app->params['managerRole']){
    $approval = 4;
    $cancel = 0;
}

?>

<div class="customer-form">
    <?php if (isset($dataApi)){ ?>
        <button type="button" class="btn btn-info btn-lg getapi" data-toggle="modal" data-target="#myModal">Get data from API</button>
    <?php } ?>
    <?php $form = ActiveForm::begin(); 
    if ($model->isNewRecord){
        $attribute = ['maxlength' => true];
    } else {
        $attribute = ['maxlength' => true, 'readonly'=>'readonly'];
    }
    ?>
    
        
    <?= $form->field($model, 'name')->textInput($attribute) ?>

    <?= $form->field($model, 'city')->textInput($attribute) ?>

    <?= $form->field($model, 'country')->textInput($attribute) ?>

    <?= $form->field($model, 'salary')->textInput($attribute) ?>

    <?= $form->field($model, 'email')->textInput($attribute) ?>
    
    <input type="hidden" id="customer-approval" name="Customer[approval]" value="<?= $approval ?>">

    <div class="form-group">
        <?php
            if ($model->isNewRecord){
                echo Html::submitButton('Create',['class'=>'btn btn-success']);
            } else {
                echo Html::submitButton('Approve',['class'=>'btn btn-primary']);
                echo "&nbsp;".Html::button('Cancel',['id'=>'cancel', 'class'=>'btn btn-danger']);
            }
        ?>
    </div>

    <?php ActiveForm::end(); ?>
    <input type="hidden" id="cancel-approval" value="<?= $cancel; ?>">
</div>
<?php if (isset($dataApi)){ ?>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Customer List</h4>
      </div>
      <div class="modal-body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataApi,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'Name',
                'City',
                'Country',
                [
                    'format'=>'raw',
                    'value' => function(){ return "<button class='btn btn-success apibtn'>Choose</button>";}
                ]
            ],
        ]); ?>
        <?php Pjax::end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php } ?>
<?php
$this->registerJs(
    "$('.getapi').click(function(){
        window.location = ".Url::to(['customer/create'])."
    });
    
    "
    );
?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
    $('.apibtn').click(function(){
        var name = $(this).closest('td').prev('td').prev('td').prev('td').text();
        var city = $(this).closest('td').prev('td').prev('td').text();
        var country = $(this).closest('td').prev('td').text();
        $('#customer-name').val(name);
        $('#customer-city').val(city);
        $('#customer-country').val(country);
        $('#myModal').modal('toggle');
    });
    $('#cancel').click(function(){
        var approval = $('#cancel-approval').val();
        $('#customer-approval').val(approval);
        $('form').submit();
    });
</script>