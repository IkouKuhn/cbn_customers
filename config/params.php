<?php

return [
    'adminEmail' => 'admin@example.com',
    'adminRole' => 1,
    'managerRole' => 2,
    'pemeriksaRoleOne' => 3,
    'pemeriksaRoleTwo' => 4,
    'customerRole' => 5
];
